export function parseStrapiError(error) {
  const message = error?.response?.data?.message;

  if (message?.length === 0) {
    return "";
  }

  return message[0].messages[0]?.message || "";
}
