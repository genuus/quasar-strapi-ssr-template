import { api } from "~/boot/axios";
import { Notify } from "quasar";
import { parseStrapiError } from "~/helpers/strapi.js";

const baseUrl = "http://localhost:1337";

export default {
  async getMainPage() {
    try {
      const response = await api.get(`${baseUrl}/main-page`);

      return response.data || undefined;
    } catch ({ response }) {
      Notify.create({
        color: "red-4",
        textColor: "white",
        position: "center",
        icon: "cloud_done",
        message: response.data,
        timeout: 10000,
        actions: [
          {
            label: "╳",
            color: "white",
            handler: () => {},
          },
        ],
      });
    }
  },
};
